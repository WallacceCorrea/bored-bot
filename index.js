const sentencesArray = {
  initial: "Are you bored?",
  sentence: [
    "Try another idea",
    "Find something to do",
    "Oh, come on! Hit the button",
    "I'm going to repeat myself",
  ],
};

const btn = document.querySelector(".get-idea-btn");
const titleEl = document.getElementById("title");
const ideaEl = document.getElementById("idea");

let isClicked = false; //check if button was clicked


const renderApp = () => {
  btn.classList.remove("inactive")
  btn.classList.add("get-idea-btn")
  titleEl.textContent = "BoredBot";
  ideaEl.style.color = "var(--text)";
  document.querySelector(".container").classList.remove("container-answer");
  isClicked ? renderData(getSentence()) : sentencesArray.initial;
};

const getActivity = () => {
  clearTimeout()
  fetch("https://apis.scrimba.com/bored/api/activity")
    .then((response) => response.json())
    .then((data) => renderActivity(data.activity));
  isClicked = true;
};

btn.addEventListener("click", getActivity);

const renderActivity = (data) => {
  btn.classList.add("inactive");
  btn.classList.remove("get-idea-btn");
  titleEl.textContent = "AttaBot!";
  ideaEl.style.color = "var(--primary)";
  document.querySelector(".container").classList.add("container-answer");
  renderData(data);
  setTimeout(renderApp, 5000)
};

const renderData = (data) => {
  document.getElementById("idea").innerHTML = data;
};

const getSentence = () => {
  let index = Math.floor(Math.random() * sentencesArray.sentence.length);
  return sentencesArray.sentence[index];
};



renderApp();
